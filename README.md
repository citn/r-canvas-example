### r-canvas简介
插件名：r-canvas</br>
插件说明：基于原生canvas绘制2D海报，canvas原生方法的二次封装，函数扩展，自定义函数，生成图片不失帧，还原真实画质，兼容性强，打造适配所有端，打造最好用的canvas插件，有问题请留言反馈，作者看到会第一时间快速修复并发版，还请留意更新公告！

### r-canvas模板列表(码云)
[r-canvas-example](https://gitee.com/citn/r-canvas-example)

### r-canvas兼容性
**<font color=red>此插件适应所有手机屏幕，承诺打造兼容所有端的r-canvas插件，待测试可以使用还请开发者们第一时间反馈，你们的支持是我更新插件的最大动力！~</font>** 

|  平台   | 是否可用  |
|  :----:  | :----:  |
| iOS | √ |
| Android | √ |
| H5或Web | √ |
| 微信小程序 | √ |
| 支付宝小程序 | 待测试 |
| 百度小程序 | 待测试 |
| 字节跳动小程序 | 待测试 |
| QQ小程序 | 待测试 |
| 快应用 | 待测试 |
| 360小程序 | 待测试 |


### 使用方式

在template中引入组件
```html
<r-canvas ref="rCanvas"></r-canvas>
```
</br>

在script中引入组件
```javascript
import rCanvas from "@/components/r-canvas/r-canvas.vue"
export default{
	components:{
		rCanvas
	}
}
```

### 使用示例
```javascript
onReady(){
	this.$nextTick(async ()=>{
		
		// 初始化
		await this.$refs.rCanvas.init({
			canvas_id:"rCanvas"
		})
		
		// 画图
		await this.$refs.rCanvas.drawImage({
			url:"/static/product_poster.png",
			x:0,
			y:0,
			w:375,
			h:630
		}).catch(err_msg=>{
			uni.showToast({
				title:err_msg,
				icon:"none"
			})
		})
		
		// 画文字
		await this.$refs.rCanvas.drawText({
			text:"精选好物",
			max_width:0,
			x:38,
			y:590,
			font_color:"rgb(175, 174, 175)",
			font_size:11
		}).catch(err_msg=>{
			uni.showToast({
				title:err_msg,
				icon:"none"
			})
		})
		
		// 生成海报
		await this.$refs.rCanvas.draw((res)=>{
			//res.tempFilePath：生成成功，返回base64图片
			// 保存图片
			this.$refs.rCanvas.saveImage(res.tempFilePath)
		})
	})
}
```

</br></br></br>

> ### r-canvas方法说明

|  方法名称   |  参数类型 | 说明  |
|  :----  | :----  | :----  |
| init  | Object |初始化canvas |
| drawImage  | Object | 画图 |
| drawText  | Object | 画文字 |
| drawRect  | Object | 画正方图形 |
| drawSpecialText  | Object | 画多样性文字 |
| fillRoundRect  | Object | 画圆角矩形 |
| clearCanvas  |  | 清空画布绘制的所有内容 |
| draw  | Function | 生成海报 |
| saveImage  | String | 保存海报 |

每个方法都有成功回调”.then()“ 和 错误回调”.catch()“，还请写上.catch捕捉错误信息方便调试错误。
</br></br></br>

> ### init(Object)方法说明

|  参数名   |  默认值 | 类型 | 是否必填 | 说明
|  :----   |  :----  | :----  | :----  |  :----  |
| canvas_id  |  | String | 是 | id唯一 |
| scale  | 1 | Number | 否 | 缩放倍数，整数数值越大，清晰度越高，图片内存越大 |
| global_alpha  | 1 | Number | 否 | 只支持0-1之间的小数，设置生成图片的整体透明度，例如：0.5 |
| canvas_width  | uni.getSystemInfoSync().windowWidth | Number | 否 | 画布宽度，默认设备屏幕宽度 | 
| canvas_height  | uni.getSystemInfoSync().windowHeight | Number | 否 | 画布高度，默认设备屏幕高度 | 
| background_color  | #ffffff | String |  否 | 画布颜色 |
| hidden  | false | Boolean |  否 | 是否隐藏画布不呈现在页面上 |

</br></br></br>

> ### drawImage(Object)方法说明

|  参数名   | 默认值 | 类型 | 是否必填 | 说明
|  :----  | :----  |  :----  | :----  | :----  |
| url  |   | String | 是 | 支持本地图片、网络路径（需支持跨域）、baes64 |
| x    |   | Number | 是 | 图片横坐标 | 
| y    |   | Number | 是 | 图片纵坐标 | 
| w    |   | Number | 是 | 图片宽度 | 
| h    |   | Number | 是 | 图片高度 | 
| border_width    |   | Number | 否 | 边框大小 | 
| border_color    |   | String | 否 | 边框颜色 | 
| is_radius    |  false | Boolean | 否 | 是否开启圆图(已废弃，请使用border_radius) | 
| border_radius    | 0 | Number | 否 | 圆角弧度，范围：0-20 | 

</br></br></br>

> ### drawText(Object)方法说明

|  参数名   | 默认值 | 类型 | 是否必填 | 说明
|  :----  | :----  |  :----  | :----  | :----  |
| text | | String | 是 | 文字内容
| x | | Number | 是 | 文字横坐标 |
| y | | Number | 是 | 文字纵坐标 |
| font_color | #000 |  String | 否 | 文字颜色 |
| font_size | 20 | Number | 否 | 文字大小 |
| font_family | Arial | String | 否 | 文字字体
| text_align | left | String | 否 | 文字对齐方式</br>left:居左</br>center:居中</br>right:居右
| max_width |  | Number | 否 | 文字到达最大宽度，会自动换行
| line_height | 20 | Number | 否 | 文字行高，设置max_width属性才能生效
| line_clamp |  | Number | 否 | 支持最大行数，设置max_width属性才能生效
| line_clamp_hint | ... | String | 否 | 超过line_clamp设置的行数则显示省略标识
| line_through_height | 0 | Number | 否 | 设置中划线高度，设置值即生效
| line_through_color | white | String | 否 | 中划线颜色
| line_through_cap | butt | String | 否 | 中划线左右两端类型</br>butt：默认</br>round：圆边</br>square：四方形边
| font_style | normal | String | 否 | 规定字体样式</br>normal：默认</br>italic：斜体的字体样式</br>oblique：倾斜的字体样式
| font_variant | normal | String | 否 | 规定字体变体</br>normal：默认</br>small-caps：小型大写字母的字体
| font_weight | normal | String | 否 | 规定字体的粗细</br>normal：默认</br>bold</br>bolder</br>lighter</br>100</br>200</br>300</br>400</br>500</br>600</br>700</br>800</br>900
| is_line_break  | false  | Boolean | 否 | 是否开启识别换行符自动换行 | 

##### drawText(Object).then回调
|  参数名  | 类型 |  说明  |
|  :----  | :----  |  :----  |
| draw_x | Number | 返回绘制的起始坐标x |
| draw_y | Number | 返回绘制的起始坐标y |
| draw_width | Number | 绘制的整体最大宽度 |
| draw_height | Number | 绘制的整体最大高度 |

##### 示例
```javascript
this.$refs.rCanvas.drawText({
	text:"精选好物",
	max_width:0,
	x:38,
	y:590,
	font_color:"rgb(175, 174, 175)",
	font_size:11
}).then(res=>{
	// res.draw_x
	// res.draw_y
	// res.draw_width
	// res.draw_height
}).catch(err_msg=>{
	uni.showToast({
		title:err_msg,
		icon:"none"
	})
})
```

</br></br></br>

> ### drawRect(Object)方法说明

|  参数名   | 默认值 | 类型 | 是否必填 | 说明
|  :----  | :----  |  :----  | :----  | :----  |
| x    |   | Number | 是 | 图形横坐标 | 
| y    |   | Number | 是 | 图形纵坐标 | 
| w    |   | Number | 是 | 图形宽度 | 
| h    |   | Number | 是 | 图形高度 | 
| color    |   | String | 否 | 图形颜色 | 

</br></br></br>

> ### drawSpecialText(Object)方法说明

general参数说明

|  参数名   | 默认值 | 类型 | 是否必填 | 说明
|  :----  | :----  |  :----  | :----  | :----  |
| x |  | Number | 是 | 文字x坐标
| y |  | Number | 是 | 文字y坐标

list参数说明

|  参数名   | 默认值 | 类型 | 是否必填 | 说明
|  :----  | :----  |  :----  | :----  | :----  |
| text | | String | 是 | 文字内容
| font_color | #000 |  String | 否 | 文字颜色 |
| font_size | 20 | Number | 否 | 文字大小 |
| margin_top | 0 | Number | 否 | 增加顶部距离
| font_family | Arial | String | 否 | 文字字体

##### 示例
```javascript
await this.$refs.rCanvas.drawSpecialText({
	general:{
		x:20,
		y:480,
	},
	list:[
		{
			text:'￥',
			font_color:'red',
			font_size:15
		},
		{
			text:'5999',
			font_color:'red',
			font_size:24
		},
		{
			text:'+',
			font_color:'rgb(255, 101, 3)',
			font_size:24,
			margin_top:2,
		},
		{
			text:'9999',
			font_color:'rgb(165, 26, 251)',
			font_size:20
		},
		{
			text:'劵',
			font_color:'rgb(165, 26, 251)',
			font_size:15
		},
	]
}).catch(err=>{
	this.error(err)
})
```

</br></br></br>

> ### fillRoundRect(Object)方法说明

|  参数名   | 默认值 | 类型 | 是否必填 | 说明
|  :----  | :----  |  :----  | :----  | :----  |
| x | | Number | 是 | 矩形横坐标 |
| y | | Number | 是 | 矩形纵坐标 |
| w | | Number | 是 | 矩形宽度 | 
| h | | Number | 是 | 矩形高度 | 
| radius | | Number | 是 | 矩形圆角弧度 | 
| fill_color | | String | 是 | 矩形颜色 | 

</br></br></br>

> ### draw(Function)方法说明

|  参数名   | 默认值 | 类型 | 是否必填 | 说明
|  :----  | :----  |  :----  | :----  | :----  |
| callback | | Function | 否 | 成功回调，也可以用.then获取成功回调 |

</br></br></br>

> ### saveImage(String)方法说明

保存图片到本地

</br></br></br>

> ### clearCanvas()方法说明

清空画布绘制的所有内容

</br></br></br>

**如果r-canvas没有你需要的函数，请反馈留言，也可以使用以下方式自行扩展canvas原生方法，谢谢理解**
```javascript
//ctx字段相当于原生canvas对象，可自行扩展canvas原生方法
this.$refs.rCanvas.ctx
```


</br></br></br>

### 学习交流微信
![交流微信](http://www.xvue.cn/rohlin-wechat-ercode.png)